#!/bin/bash

sudo apt-get install python3-dev python3-setuptools python3-numpy python3-opengl libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev libsmpeg-dev libsdl1.2-dev libportmidi-dev libswscale-dev libavformat-dev libavcodec-dev libtiff5-dev libx11-6 libx11-dev fluid-soundfont-gm timgm6mb-soundfont xfonts-base xfonts-100dpi xfonts-75dpi xfonts-cyrillic fontconfig fonts-freefont-ttf libfreetype6-dev pyqt5-dev-tools

cd ~/source
# Artemis
echo "Installing Artemis SigInt analytics"
if [ ! -d ~/source/artemis-fork ]; then
    git clone https://gitlab.com/crankylinuxuser/artemis-fork
    cd artemis-fork
else
    cd artemis-fork
    git pull
fi
cd requirements
pip3 install -r  requirements.txt --user
cd ~/source
